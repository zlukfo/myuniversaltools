# -*- coding: utf-8 -*-
import sys
# при необходимости, добавляем путь к функциям, выпол
sys.path.append('/path/to/command/functions')

from webob import Request
import simplejson
import re
import types
# здесь подключаются модули коммандных функций -------->>
from commandFunctions import plainData, newHandle
#	......
#<<----------------

def application(environ, start_response):

	req = Request(environ, charset='utf8')
	#??????
	command_chain=req.path_info.split('/')[1:] #первым элементом в цепочке будет 'command', далее - непосредственно команда
	params=dict(req.params.items())	

	# здесь 1) определяем список допустимых команд (другие будут возвращать пустой ответ)
	# 2) подвязываем к каждой команде функцию (из модулей, импортированных ранее)
	available_commands=[
		('plain_data', plainData),
		('new_handle', newHandle)
		#	...
	]

	# шаблон HTTP-ответа по умолчанию. Если предполагается изменить статус или расширить заголовки
	# это можно сделать в функции, исполняющей команду (см пример commandFunctions.py ->newHandle)	
	response={
		'__status':'200 OK',
		'__headers': 	[
						('Access-Control-Allow-Origin','*'),
						('Content-type', 'application/json; charset=utf-8'),
            		],
        'data':['']
	}

	output=[]
	for command, func in available_commands:
		if command==command_chain[0]:
			# ниже в списке указываются команды, идущие из доверенных источников, по кототым НЕ 
			# нужно делать проверку запроса на право исполнения команды. Если таких команд нет - список пустой 
			if not command in ['no_auth']:
			    # если в функцию нужно передать значение кукисов от клиента, можно например так
			    # добавить их в словарь параметров
			    params['cookie_email']=req.cookies.get('email', '') 
			    #	...

			    # проверяем право запроса на выполнение команды (объявление функции см ниже)
			    # механизм реализации проверки может быть разный (описывается в теле validRule)
			    # соотвественно, в зависимости от принятого механизма НИЖЕ УКАЗАТЬ нужные аргументы
			    if not validRule(*args, **kwargs):
					# формат ответа о НЕподстверждении прав соотвествует
					# нашему соглашению об 'отрицательном' результате работы функций
					output={"type_message":"error", "message": "Autorization error"}
					break
			
			# для прошедших проверку и 'доверенных' запросов вызываем функцию команды
			output=func(command_chain, params, environ)
			break

	# обрабазываем новые заголовки ответа, переданные функцией
	if type(output)==type({}) and ('__status' in output or '__headers' in output):
		response['__status']=output.get('__status', response['__status']) 
		response['__headers']=output.get('__headers', response['__headers']) 
		response['data']=[simplejson.dumps(output.get('data', response['data']))] 
	else:
		# если ответом возвращается генератор cобираем значения в массив
		if isinstance(output, types.GeneratorType):
			output=[i for i in output]
		response['data']=[str(output)]

	response['__headers'].append(('Content-Length', str(len(response['data'][0]))))

	start_response(response['__status'], response['__headers'])
	return response['data']

# функция в которой реализуется механизм проверки запроса на право выполнения команды
# функция должна возвращать True или False
def validRule(*args, **kwargs):
	return True




