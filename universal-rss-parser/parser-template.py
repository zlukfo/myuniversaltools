# -*- coding: utf-8 -*-
from xmlIterParser import xmlParser
#import codecs
import psycopg2
#import os
import datetime
#import sys
from dateutil.parser import parse

url='http://minfin.ru/common/rss_news.php?lid=1&amp;sid=1&amp;mod=lib&amp;lim=1000'
connect={
	'dbname': 'parser',
	'user': 'postgres',
	'passwd': 'qweasd'

}
schema='minfin_rss'

def connect_db():
	try:
		conn = psycopg2.connect("dbname='%(dbname)s' user='%(user)s' password='%(passwd)s'" % connect)
		conn.autocommit=True
		cur = conn.cursor()
	except:
		sys.stderr.write('Not connection to db')
		return -1
	return cur

-------------------
sql_filename=(('create-schema.sql', 'Подготавливаю базу данных...'),
	)

try:
	conn = psycopg2.connect("dbname='%(dbname)s' user='%(user)s' password='%(passwd)s'" % connect)
except:
    print "Не могу подключиться к базе данных"
cur = conn.cursor()

env = Environment(loader = FileSystemLoader(''))
for script in sql_filename:
	print script[1]
	template = env.get_template(script[0])
	sql=template.render(config).encode('utf-8')
	cur.execute(sql)
conn.commit()
conn.close()
--------------------


if __name__=='__main__':

	cur=connect_db()
	if cur != -1:
		x=xmlParser(url)
		record_values={}
		for i in x.getData(['item/title', 'item/description', 'item/link', 'item/pubDate']):
			if len(record_values)==4:
				record_values['schema']=schema
				q="INSERT INTO %(schema)s.rss_main (title, descript, pub_date, key_d_t, fts) VALUES ('%(title)s','%(description)s','%(pubDate)s'::timestamp with time zone, md5('%(title)s' || '%(description)s'), to_tsvector('%(title)s' || '%(description)s'));" % record_values
				try:
					cur.execute(q)
				except:
					pass
				record_values={}
			if i[0]=='rss/channel/item/pubDate':
				i[1]=parse(i[1]).strftime('%Y-%m-%d %H:%M:%S%z')
			record_values[i[0].split('/')[-1]]=i[1]
