# -*- coding: utf-8 -*-
import simplejson
import psycopg2 
import re

DB_NAME='parser'
CONFIG_SCHEMA='config'
def _getConfig(user_param=''):
	'''
	Function: getConfig
	Summary: Возвращает имя файла, содержащего набор доступныз sql запросов по параметру user_param, переданному пользователем. Если параметры будут храниться не в файле а в бд - функцию нужно переписать
	Examples: InsertHere
	Attributes:		@param (user_param) default='': InsertHere
	Returns: InsertHere
	'''
	user_param=user_param.get('sql_theme', None)
	if not user_param: 
		return None
	CONFIG_SOURCE='sql.conf'
	f=open(CONFIG_SOURCE,'r')
	conf=simplejson.load(f)
	f.close()
	if user_param in conf:
		return conf[user_param]
	return None
def _sqlList(sql_source=""):
	'''
	Function: sqlList
	Summary: возвращает из файла список доступных запросов по sql-источнику. если источник бд - функцию нужно переписать
	Examples: InsertHere
	Attributes: 
		@param (sql_source) default="": InsertHere
	Returns: InsertHere
	'''
	if not sql_source:
		return None
	if TYPE_SOURCE=='text':
		try:
			f=open(sql_source,'r')
		except Exception as e:
			return {'type':'error', 'message':'Source type - "text". Source not found'}
		sql_list=simplejson.load(f)
		f.close()
	if TYPE_SOURCE=='db':
		try:
			# внимание! если заходим под другим пользователем бд - поменять
			conn = psycopg2.connect("dbname=%s user=postgres" % (sql_source,))
			cur = conn.cursor()
			# внимание! если схема и таблица называются по другому - поменять
			cur.execute('SELECT config.request_sql_list.*, config.projects.project_name FROM config.request_sql_list LEFT OUTER JOIN config.projects ON (config.request_sql_list.projects_id = config.projects.id);')
			sql_list=cur.fetchall()
			conn.close()
		except Exception as e:
			return {'type':'error', 'message':'Source type - "db". Source not found'}

	return sql_list

def _getUserGroup(request):
	'''
	Function: _getUserGroup
	Summary: прототип функции проверки пользователя. Данная ф. должна возвращать группу пользователя или None. Пользователей нужно будет хранить в бд, пароли в хеше ... почитать описание
	Examples: InsertHere
	Attributes: 
		@param (request):InsertHere
	Returns: InsertHere
	'''
	user=request.get('user', None)
	password=request.get('password', None)
	if not user or not password:
		return None

	#--получаем карточу польхователя если найден или None если нет
	# при хранении пользователей в бд этот кусок переписать
	f=open("users.json",'r')
	users=simplejson.load(f)
	f.close()
	user=None
	for i in users:
		if i['user']==request['user']:
			user=i
			break
	#print user
	#---------------------------------------------------------

	#здесь еше проверка пароля и возвращать группу только если совпадает
	# .. доп действия с карточкой пользователя

	return user['group']

def checkRequest(request):
	'''
	Function: checkRequest
	Summary: проверяет запрос и его параметры на право выполнения
	Examples: InsertHere
	Attributes: 
		@param (request):словарь с данными пользовательсого запроса
	Returns: в случае успеха - sql запрос с подставленными значениями параметров, в случае неудачи - None или словарь с описанием ошибки
	'''
	sql_config_source=_getConfig(request)
	sql_list=''
	# проверяем, существует ли источник со списоком доступных запросов для проекта к которому обращается пользовательский запрос
	if not sql_config_source:
		return None
	# пытаемся получить список доступных запросов
	sql_list=_sqlList(sql_config_source)
	if not sql_list:
		return None
	#оределяем принадлежность пользовалеля к группе
	user_group=_getUserGroup(request)
	if not user_group:
		return None
	#проверяем существование команды, запрошенной пользователем
	tmp=[i['alias'] for i in sql_list]
	if not request['query'] in tmp:
		return {'type':'error', 'message':'Command not found'}
	else:
		ind=tmp.index(request['query'])
	# проверяем привелегии на выпонение запроса
	# если для запроса группа не указана - запрос не разрешен
	if user_group not in sql_list[ind]['privilege'].keys():
		return {'type':'error', 'message':'Requert not permitted'}		
	# проверяем наличие всех необходимых значений для выполнения запроса
	p_fact=[k for k,v in request['params'].items()]
	# данная строка определяем условия написания пременных параметров в шаблоне запроса
	# названия параметров состоят только из строчных латиниских букв и _ и с каждой стороны окружены ##
	p_need=[i[2:-2] for i in re.findall(r'##[a-z_]+##', sql_list[ind]['sqlquery'])]
	if not set(p_fact)==set(p_need):
		return None
	q=sql_list[ind]['sqlquery']
	for k,v in request['params'].items():
		#подавляем все недопустимые символы в значениях параметров
		v=re.sub(r'[^a-zA-Zа-яА-ЯЁё0-9_\-\ \*]','',str(v))
		if v.isdigit():
			q=q.replace('##'+k+'##', v)
		q=q.replace('##'+k+'##', "'"+v+"'")
	q+=' limit %d' %(sql_list[ind]['privilege'][user_group])
	 
	return q


if __name__=='__main__':
	user_request={
		'project':'zakupki',
		'query':'getRecords',
		'params':{
			'city':'hhhj!8_)kkk',
			'name':78
		},
		# по ним определяется группа а затем и привелегии
		'user':'user',
		'password':'j'
	}
	conn = psycopg2.connect("dbname=%s user=parser" % ('parser',))
	cur = conn.cursor()
	cur.execute("""select sql_quesy from config.request_sql_list 
INNER JOIN config.user_password_group_project
ON (config.request_sql_list.projects_id=config.user_password_group_project.id_project)
where 
	user_name='temp_user' 
	and password='password'
	and project_name='zakupki'
	and alias='getRecords'""")
	sql_list=cur.fetchone()
	if sql_list:
		print sql_list[0]

