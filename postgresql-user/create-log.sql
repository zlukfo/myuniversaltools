--создаем схему для логов
create schema logs; 

--создаем мастер-таблицу логированая запросов
	CREATE TABLE logs."log_requests" (
		"id" serial NOT NULL,
		"request_time" timestamp with time zone NOT NULL,
		"user_name" TEXT NOT NULL,
		"request_alias" TEXT NOT NULL,
		"project" TEXT NOT NULL,
		"passwd" TEXT NOT NULL,
		CONSTRAINT log_requests_pk PRIMARY KEY ("id")
	) WITH (
	  OIDS=FALSE
	);

-- процедура и триггер обеспечивающая запись (по текущей дате)  лога пользователя в нужную таблицу
CREATE OR REPLACE FUNCTION logs.add_log() RETURNS TRIGGER AS $$
DECLARE
    table_master    varchar(255)        := 'log_requests';
    table_part      varchar(255)        := '';		
BEGIN
    -- задаем имя дочерней таблицы
    table_part := table_master 
                    || '_y' || DATE_PART( 'year', NEW.request_time)::TEXT 
                    || '_m' || DATE_PART( 'month', NEW.request_time)::TEXT; 
    
    -- Проверяем дочернюю таблицу на существование 
    PERFORM 1 FROM pg_class WHERE relname = table_part LIMIT 1;
 
    -- Если её ещё нет, то создаём
    IF NOT FOUND THEN
        -- Cоздаём партицию, наследуя мастер-таблицу
        EXECUTE '
            CREATE TABLE ' || TG_TABLE_SCHEMA || '.' || table_part || ' ( )
            INHERITS ( '|| 'logs.' || table_master || ' )
            WITH ( OIDS=FALSE )';
        -- Создаём индексы для вновь созданной таблицы
        
        --EXECUTE '
        --    CREATE INDEX ' || table_part || '_adid_date_index
        --    ON ' || table_part || '
         --   USING btree
         --   ("docPublishDate")';
         
    END IF;
 
    -- добавляем запись лога в нужную дочернюю таблицу
    EXECUTE '
        INSERT INTO ' || TG_TABLE_SCHEMA || '.' || table_part || '
        SELECT ( (' || QUOTE_LITERAL(NEW) || ')::' || TG_TABLE_SCHEMA || '.' || TG_TABLE_NAME || ' ).*;';
 
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

	CREATE TRIGGER add_log_trigger
	BEFORE INSERT ON logs.log_requests
	FOR EACH ROW EXECUTE PROCEDURE logs.add_log();