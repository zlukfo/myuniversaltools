-- создаем материализованное представление
CREATE MATERIALIZED VIEW {{config_schema}}.rsl_p_u_pg as
	select rsl.id as rsl_id, rsl.alias, rsl.sql_query, rsl.privilege, p_u_pg.* from {{config_schema}}.request_sql_list as rsl join
		(select * from {{config_schema}}.projects as p join
			(select u.id as u_id, u.user_name, u.password_hash, pg.id as pg_id, pg.group_name , pg.project_id from {{config_schema}}.users as u join {{config_schema}}.privilege_groups as pg on(u.group_id=pg.id)) as u_pg
		on (p.id=u_pg.project_id)) as p_u_pg
	using (project_id)
	order by (project_name, user_name);

-- триггерная процедура и триггеры автоматического обновления
	CREATE OR REPLACE FUNCTION {{config_schema}}.update_view() RETURNS TRIGGER AS $$
	BEGIN
	REFRESH MATERIALIZED VIEW {{config_schema}}.rsl_p_u_pg;
	RETURN NULL;
	END;
	$$ LANGUAGE plpgsql;

	CREATE TRIGGER update_view_trigger
	AFTER INSERT OR UPDATE OR DELETE ON {{config_schema}}.users
	FOR EACH STATEMENT EXECUTE PROCEDURE {{config_schema}}.update_view();

	CREATE TRIGGER update_view_trigger
	AFTER INSERT OR UPDATE OR DELETE ON {{config_schema}}.projects
	FOR EACH STATEMENT EXECUTE PROCEDURE {{config_schema}}.update_view();

	CREATE TRIGGER update_view_trigger
	AFTER INSERT OR UPDATE OR DELETE ON {{config_schema}}.privilege_groups
	FOR EACH STATEMENT EXECUTE PROCEDURE {{config_schema}}.update_view();

	CREATE TRIGGER update_view_trigger
	AFTER INSERT OR UPDATE OR DELETE ON {{config_schema}}.request_sql_list
	FOR EACH STATEMENT EXECUTE PROCEDURE {{config_schema}}.update_view();