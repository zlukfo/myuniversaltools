/*
	здесь в таблицы вставляется наборы тестовых данных
*/
SET search_path TO "config7";

alter sequence privilege_groups_id_seq restart;
alter sequence projects_id_seq restart;
alter sequence request_sql_list_id_seq restart;
alter sequence users_id_seq restart;

-- таблица проектов
INSERT INTO projects(project_name) VALUES ('zakupki');
INSERT INTO projects(project_name) VALUES ('avito');
INSERT INTO projects(project_name) VALUES ('rss');

--таблица групп превилений
INSERT INTO privilege_groups(group_name, project_id) VALUES ('guest', 1);
INSERT INTO privilege_groups(group_name, project_id) VALUES ('vip', 1);
INSERT INTO privilege_groups(group_name, project_id) VALUES ('admin', 1);

INSERT INTO privilege_groups(group_name, project_id) VALUES ('guest', 2);
INSERT INTO privilege_groups(group_name, project_id) VALUES ('user_group', 2);
INSERT INTO privilege_groups(group_name, project_id) VALUES ('admin', 2);

INSERT INTO privilege_groups(group_name, project_id) VALUES ('guest', 3);
INSERT INTO privilege_groups(group_name, project_id) VALUES ('users', 3);

-- таблица запросов
INSERT INTO request_sql_list(alias, sql_query, privilege, project_id)
    VALUES ('getCount', 'select * from config.users where user_name=%L', '{"vip":1,"admin":2, "anonymous":7}'::json, 1);
INSERT INTO request_sql_list(alias, sql_query, privilege, project_id)
    VALUES ('getCount', 'b', '{"user_group":1,"admin":2}'::json, 2);
INSERT INTO request_sql_list(alias, sql_query, privilege, project_id)
    VALUES ('getCount', 'c', '{"guest":1,"users":2, "anonymous":8}'::json, 3);
INSERT INTO request_sql_list(alias, sql_query, privilege, project_id)
    VALUES ('getAllRecords', 'd', '{"admin":20, "anonymous":2}'::json, 1);
INSERT INTO request_sql_list(alias, sql_query, privilege, project_id)
    VALUES ('getAllRecords', 'e', '{"admin":10}'::json, 2);

--таблица пользователей

INSERT INTO users(user_name, password_hash, group_id)
VALUES ('admin', 'p1', 6);
INSERT INTO users(user_name, password_hash, group_id)
VALUES ('admin', 'p2', 9);
INSERT INTO users(user_name, password_hash, group_id)
VALUES ('guest', 'g1', 4);
INSERT INTO users(user_name, password_hash, group_id)
VALUES ('guest', 'g4', 7);
INSERT INTO users(user_name, password_hash, group_id)
VALUES ('guest', 'g7', 10);
INSERT INTO users(user_name, password_hash, group_id)
VALUES ('jon', 'g1', 5);
INSERT INTO users(user_name, password_hash, group_id)
VALUES ('ivan', 'g1', 5);
INSERT INTO users(user_name, password_hash, group_id)
VALUES ('petr', 'g4', 8);
INSERT INTO users(user_name, password_hash, group_id)
VALUES ('ivan', 'g7', 11);
