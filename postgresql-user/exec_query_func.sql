-- Создаем функции для выполнения пользовательских запросов

-- функция возвращает sql-шаблон запроса, найденный по параметрам пользователя	
	CREATE OR REPLACE FUNCTION {{config_schema}}.getQuery(usr text, passwd text, project text, al text) RETURNS text AS $$
		DECLARE
			r RECORD;
		BEGIN
			 INSERT INTO logs.log_requests(request_time, user_name, request_alias, project, passwd)
    			VALUES (now(), usr, al, project, passwd);
			select * into r from
				(select json_object_keys(privilege) as g_n, group_name, sql_query
				 from {{config_schema}}.rsl_p_u_pg as t
					where
						user_name=usr and
						password_hash=passwd and
						project_name=project and
						alias=al
				) as a
			where g_n=group_name;
		-- если найден запрос для авторизованного пользователя
		if r.sql_query <> '' then
			RETURN r.sql_query;
		end if;
		-- проверяем существут ли такой запрос для анонимного пользователя
			select * into r from
				(select json_object_keys(privilege) as g_n, group_name, sql_query
				 from {{config_schema}}.rsl_p_u_pg as t
					where
						project_name=project and
						alias=al
				) as a
			where g_n='anonymous'
			limit 1;
		-- если существует - возвращаем
		if r.sql_query <> '' then
			RETURN r.sql_query;
		else
			--RAISE EXCEPTION 'Права пользователя не разрешают выпольнить запрос или такого запроса не существует';
			RETURN NULL;
		end if;
		END;
	$$ LANGUAGE plpgsql;

--функция подстановки значений параметров пользователя в sql-шаблон по параметрам пользователя
-- шаблон находится через функцию getQuery()
CREATE OR REPLACE FUNCTION {{config_schema}}.getFromTempl(templ text, VARIADIC args text[]) RETURNS text  AS $$
	declare 
		r record;
	begin
		execute format('select format(''%s'', %s) as res', templ, '''' ||array_to_string(args,''', ''')||'''') into r; 
		return r.res;
	end;
	$$ LANGUAGE plpgsql;

-- функция выполнения запроса по значениям параметров пользователя 
-- Независимо от объема полученных данных весь результат будет свернут в один массив json 
--(в результате работы execQuery вернет отношение из дной строки и обного столбца). 
--все результаты пользовательского запроса будут в этой ячейке.
CREATE OR REPLACE FUNCTION {{config_schema}}.execQuery(par text) RETURNS json   AS $$
	DECLARE
		res record;
	BEGIN
		execute format('select array_to_json(array_agg(t)) as result from (%s) as t', par) into res;
		return res.result; 
	END;
	$$ LANGUAGE plpgsql;

--select config.execQuery(config.getfromtempl(config.getQuery('admin', 'p1', 'zakupki', 'getCount'), 'admin', 'asdasda'))