--ЗДЕСЬ СОЗДАЕТСЯ БАЗА ДАННЫХ, СХЕМА КОНФИГУРАЦИИ, СКРИПТЫ И СВЯЗИ

-- создаем базу данных
--create database {{database}};

--создаем схему 
create schema {{config_schema}}; 

-- создаем таблицу пользователей пользователи
CREATE TABLE {{config_schema}}."users" (
	"id" serial NOT NULL,
	"user_name" TEXT NOT NULL,
	"password_hash" TEXT NOT NULL,
	"email" TEXT,
	"group_id" bigint NOT NULL,
	CONSTRAINT users_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

--ограничение обеспечивающее уникальность принадлежности пользователя с заданным именем к паре проект+группа привелегий. чтобы к проекту нельзя было добавить нескольких пользователей с одинаковым именем и привелегиями
ALTER TABLE {{config_schema}}.users
  ADD CONSTRAINT user_group_tuple_uniq UNIQUE (user_name, group_id);


--триггер, который перед операциями в таблице пользователей, изменении его имени или группы привелегий проверяет, существует ли такой пользователь в данном проекте. если существует, операция не выполняется. Это нужно для того, чтобы нельзя было добавить к проекту нескольких пользователей с одинаковым именем но разными превилегиями

-- создаем таблицу проектов 
CREATE TABLE {{config_schema}}."projects" (
	"id" serial NOT NULL,
	"project_name" TEXT NOT NULL UNIQUE,
	CONSTRAINT projects_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

--создаем таблицу запросов
CREATE TABLE {{config_schema}}."request_sql_list" (
	"id" serial NOT NULL,
	"alias" TEXT NOT NULL,
	"sql_query" TEXT NOT NULL,
	"privilege" json NOT NULL,
	"project_id" bigint NOT NULL,
	CONSTRAINT request_sql_list_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

--связь-ссылка запроса с поектом
ALTER TABLE {{config_schema}}."request_sql_list" ADD CONSTRAINT "request_sql_list_fk0" FOREIGN KEY ("project_id") REFERENCES {{config_schema}}."projects"("id");

--ограничение, допускающее только уникальные пары (псевдоним запроса, id проекта) запрос относится только к одному проекту, но у разных проектов могут быть запросы с одинаковым псевдонимом 
ALTER TABLE {{config_schema}}.request_sql_list
  ADD CONSTRAINT alias_project_tuple_uniq UNIQUE (alias, project_id);

--триггерная функция и триггер, проверяющий перед операциями INSERT, UPDATE в таблицу request_sql_list правильность имен групп привелегий для конкретного проекта
CREATE OR REPLACE FUNCTION {{config_schema}}.check_groups() RETURNS TRIGGER AS $$
    BEGIN
	IF 
		array(select json_object_keys(NEW.privilege))
		<@
		array(select group_name from {{config_schema}}.projects as p join {{config_schema}}.privilege_groups as g on(p.id=g.project_id) where p.id=NEW.project_id)
	    =false THEN
		RAISE EXCEPTION 'Указанные имена групп превилегий не соответствуют существующим в заданном проекте';
		RETURN NULL;
	END IF;
	RETURN NEW;
   END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER check_groups_trigger
BEFORE INSERT OR UPDATE ON {{config_schema}}.request_sql_list
FOR EACH ROW EXECUTE PROCEDURE {{config_schema}}.check_groups();


--создаем таблицу группы превилегий
CREATE TABLE {{config_schema}}."privilege_groups" (
	"id" serial NOT NULL UNIQUE,
	"group_name" TEXT NOT NULL,
	"project_id" bigint NOT NULL,
	CONSTRAINT privilege_groups_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

--связь-ссылка группы с проектом
ALTER TABLE {{config_schema}}."privilege_groups" ADD CONSTRAINT "privilege_groups_fk0" FOREIGN KEY ("project_id") REFERENCES {{config_schema}}."projects"("id");

--ограничение, допускающее только уникальные пары (имя группы, id проекта)
--группа привелегий относится только к одному проекту, но у разных проектов могут быть группы с одинаковыми именами
ALTER TABLE {{config_schema}}."privilege_groups"
  ADD CONSTRAINT group_project_tuple_uniq UNIQUE (group_name, project_id);

--связь-ссылка пользователя с группой привелегий (фактически с парой группа+проект)
ALTER TABLE {{config_schema}}."users" ADD CONSTRAINT "users_fk0" FOREIGN KEY ("group_id") REFERENCES {{config_schema}}."privilege_groups"("id");

--триггер, который перед операциями пользователя, изменении его имени или группы привелегий проверяет, существует ли такой пользователь в данном проекте. если существует, операция не выполняется. Это нужно для того, чтобы нельзя было добавить к проекту нескольких пользователей с одинаковым именем но разными превилегиями
CREATE OR REPLACE FUNCTION {{config_schema}}.check_user_in_projects() RETURNS TRIGGER AS $$
    BEGIN
	IF 
		-- в какой проект мы хотим зарегистрировать пользователя
		(select project_id from {{config_schema}}.privilege_groups where id=NEW.group_id)
		
		=any(
		--в каких проектах пользователь с данным именем уже зарегистрирован
		array (select project_id from {{config_schema}}.users as u join {{config_schema}}.privilege_groups as pg on (u.group_id=pg.id) 
			where u.user_name=NEW.user_name)
	)
	    =true THEN
		RAISE EXCEPTION 'Пользователь с таким именем уже существует в проекте. И имеет определенные привелегии';
		RETURN NULL;
	END IF;
	RETURN NEW;
   END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER check_user_in_projects_trigger
BEFORE INSERT OR UPDATE ON {{config_schema}}.users
FOR EACH ROW EXECUTE PROCEDURE {{config_schema}}.check_user_in_projects();



--автоматически добавляем группу анонисусов для каждого проекта
	CREATE OR REPLACE FUNCTION {{config_schema}}.addAnonymous() RETURNS TRIGGER AS $$
		BEGIN
			 INSERT INTO {{config_schema}}.privilege_groups(group_name, project_id)
				VALUES ('anonymous', NEW.id);
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

	CREATE TRIGGER addAnonymous_trigger
	AFTER INSERT ON {{config_schema}}.projects
	FOR EACH ROW EXECUTE PROCEDURE {{config_schema}}.addAnonymous();