#!/usr/bin/python
# -*- coding: utf-8 -*-
from jinja2 import Environment, FileSystemLoader
import psycopg2
from psycopg2.extras import Json 
import hashlib
import string
import argparse

#----КОНСТАНЫ
SQL_SCRIPT=(('create-objects.sql', 'Создаем базу данных и таблицы конфигурации...'),
				('query_check.sql', 'Создаем материализованное представление...'),
				('exec_query_func.sql', 'Функции проверки и выполнения запросов пользователя...'),
				('create-log.sql', 'Инструменты логирования запросов...'),
				('user_role.sql', 'Создаем пользователя для запросов. ВНИМАНИЕ!!! 1) Не забудьте закрыть доступ к схемам с данными проектов. 2) запретить подключение к другим базам данных'),
				('get_user_rules.sql', '')
		)

#------СЛУЖЕБНЫЕ ФУНКЦИИ
def _execSqlScript(sql_filename='', render_param={}):
	'''
	Function: _runSqlScript
	Summary: выполняет все ИЛИ заданные sql-скрипты, определенные в константе SQL_SCRIPT
	Attributes: 
		@param (sql_filename) default='': sql-скрипт (стока) или список скриптов для выполнения
		@param (render_param) default={}: словарь переменных, значения которых нужно подставить в sql-скрипты
	Returns: 0 - в случае неудачного исполнения функции, курсор подколючения к бд - в случае удачного
	'''
	if type(sql_filename)==type(''):
		sql_filename=[sql_filename]
	if type(sql_filename)!=type([]):
		print 'Error in _runSqlScript(): Second param must be string or list'
		return
	if type(render_param)!=type({}):
		print 'Error in _runSqlScript(): 3-th param must be dict'
		return
	env = Environment(loader = FileSystemLoader(''))
	for script in SQL_SCRIPT:
		if sql_filename==[''] or script[0] in sql_filename:
			print script[1]
			template = env.get_template(script[0])
			sql=template.render(render_param).encode('utf-8')
			#cur.execute(sql)
			yield _execSqlQuery(sql, render_param['database'])

def _execSqlQuery(query_string, dbname=''):
	'''
	Function: _execSqlQuery
	Summary: подключается к серверу бд (или конкретной бд) и выполняет запрос
	Examples: InsertHere
	Attributes: 
		@param (query_string): строка запроса
		@param (dbname) default='': если параметр не задан - подключается к серверу, если задано имя бд - пробует подключиться к ней
	Returns: в случае успеха возвращае все сроки результатов запроса
	'''
	
	db_config['database']=dbname
	try:
		# !!! непонятка - если пароль пользователя бд задан неверно - try все равно отрабазывается и бд создается
		if dbname:
			conn = psycopg2.connect("dbname=%(database)s user=%(dbuser)s host=%(host)s password=%(passwd)s port=%(port)s" % 
			    	db_config)		
		else:
			conn = psycopg2.connect(" user=%(dbuser)s host=%(host)s password=%(passwd)s port=%(port)s" % 
			    	db_config)					
		conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
		cur = conn.cursor()
		cur.execute(query_string)
		conn.commit()
	except psycopg2.Error as e:
		print "ERROR: %s, %s" % (e.pgcode, e.pgerror)
		return []
	
	result=1
	if type(cur.description)!=type(None):
		result=cur.fetchall()
	conn.close()
	return result

def _generatePrivilege(group_name, privilege):
	'''
	Function: _generatePrivilege
	Summary: Выполняет проверку  задаваемых привелегий
	Attributes: 
		@param (group_name):InsertHere
		@param (privilege):должен быть словарь {'count':_, 'timeout':_, 'record':_}
			прочерки - целые числа. стоварь не обязательно полный, только те ключи для которых 
			хотим задать значения остальные будут 0 - ограничение по данному типу привелегии отсутсвует 
	Returns: возврвщает объект привелегий, адаптированный для добавления в запрос 
			в случае ошибки - пустой словарь
	'''
	if not privilege or type(privilege)!=type({}):
		print 'ВНИМАНИЕ: Привелегии не заданы или заданы неверно'
		return {}
	
	if privilege and not (set(privilege.keys()) <= set(['count', 'timeout', 'record'])):
		print "ERROR: Privilege must have only keys 'count', 'timeout', 'record'"
		return {}	
	p={'count':0, 'timeout':0, 'record':0}
	for k,v in privilege.items():
		try:
			v=int(v)
		except:
			print "ERROR: Privilege values must be integer"
			return []
		p[k]=v
	return {group_name:p}

def _parseRules(rules):
	'''
	Function: _parseRules
	Summary: парсит привелегии запроса из командной строки. в командной строке привилегии должны
			задаваться примерно так 'count=3, val=2, ....'
			функция исключает элементы, значения которых не числовые данные (см. что такое привелегии и как задаются привелегии)
	Returns: возварщает привелегии, заданные пользователем в командной строке в виде СЛОВАРЯ
			! не факт, что правильно заданные, дальнейшаю проверка будет выполнена _generatePrivilege
	'''
	if not rules or type(rules)!=type(''):
		return {}
	rules=rules.replace(' ','').split(',')
	rules=dict([tuple(i.split('=')) for i in rules if '=' in i])
	rules={k:v for k,v in rules.items()}
	return dict(rules)

# ------ОСНОВНЫЕ ФУНКЦИЯ СОЗДАНИЯ И КОНФИГУРИРОВАНИЯЯ ПРИЛОЖЕНИЯ
def createConfigApplication(args):
	'''
	Function: createConfigApplication
	Summary: создает новое web-приложение. вся логика работы функции выполняется в бд sql-скриптами
			для изменения поверения - менять скрипты sql
	Returns: 1 - в случае успешного завершения None - не успешного 
	'''
	if _execSqlQuery('create database %s' % (args.appname,)):
		# !!! некрасивый код, но генератор нужно перебрать !
		for i in _execSqlScript(render_param={'database':args.appname, 'config_schema':args.confname, 'db_project_user':args.user, 'db_project_password':args.password}):
			pass
	return []

def getUserApplicationRules(args):
	'''
	Function: getUserApplicationRules
	Summary: работает в двух режимах
		1) вызов без параметров - возвращает имена всех бд кластера
		2) вызов с двумя параметрами -возврашает права доступа пользователя для объектов БД
	'''
	res=_execSqlQuery('select datname from pg_database;')
	if not args.db_user:
		return [i[0] for i in res]
	if not args.appname in [i[0] for i in res]:
		print "Database '%s' is not exists" % (args.appname,)
		return []

	res=_execSqlQuery('select usename from pg_user;')
	if not args.db_user in [i[0] for i in res]:
		print "User '%s' is not exists" % (args.db_user,)
		return []
	res=_execSqlScript('get_user_rules.sql', {'database':args.appname, 'db_user':args.db_user})
	return res

def addProject(args):
	'''
	Function: addProject
	Summary: добавляет новый проект
	'''
	res=_execSqlQuery("select table_schema from information_schema.tables where table_name='projects'", args.appname)

	if res:
		print "INFO: For application '%s' config name is '%s'\n" % (args.appname, res[0][0])
	_execSqlQuery("INSERT INTO %(config_schema)s.projects (project_name) VALUES ('%(project_name)s')" % {'project_name':args.project,'config_schema':args.confname}, args.appname)
	_execSqlQuery("CREATE SCHEMA %(project_name)s" % {'project_name':args.project}, args.appname)

	return []

def addGroup(args):
	'''
	Function: addGroup
	Summary: добавляет к проекту новую группу
	Returns: InsertHere
	'''
	if not args.group:
		print "ERROR: Group name must be define"
		return []
	print "INFO: Add group to application '%s'\n" % (args.appname)
	res=_execSqlQuery("select id from  %s.projects where project_name='%s'" % (args.confname, args.project), args.appname)
	if not res:
		print "ERROR: Project '%s' is not exists" % (args.project,)
		return []
	_execSqlQuery("INSERT INTO %(config_schema)s.privilege_groups (group_name, project_id) VALUES ('%(group_name)s', '%(project_id)s')" % {'group_name':args.group,'config_schema':args.confname, 'project_id':res[0][0]}, args.appname)
	return []

def addUser(args):
	'''
	Function: addGroup
	Summary: добавляет к проекту новую группу
	Returns: InsertHere
	'''	
	if not args.user:
		print 'ОШИБКА: Необходимо задать имя пользователя'
		return []
	print "INFO: Add user to application '%s'\n" % (args.appname)
	if not args.password:
		print 'WARNING: Password is not ...'
	m = hashlib.sha256()
	m.update(args.password)
	args.password=m.hexdigest()
	if not args.mail:
		print 'WARNING: Email is not ...'
	res=_execSqlQuery("select id from  %s.privilege_groups where group_name='%s'" % (args.confname, args.group), args.appname)

	if not res:
		print "ERROR: Group '%s' in project '%s' is not exists" % (args.group, args.appname)
		return []
	_execSqlQuery("INSERT INTO %(config_schema)s.users (user_name, password_hash, email, group_id) VALUES ('%(user_name)s', '%(password)s', '%(email)s', %(group_id)s)" % {'config_schema':args.confname, 'user_name':args.user, 'password':args.password, 'email':args.mail, 'group_id':res[0][0]}, args.appname)
	return []

def addQuery(args):
	'''
	Function: addQuery
	Summary: InsertHere
	Attributes: 	
		@param (privilege) default={}: Не обязательно сразу определять привелегии для добавляемого пользователя. Это можно сделать позже
	Returns: InsertHere
	'''
	# сочетание существующий алиас+отсутсвие шаблона указывает, то мы хотим изменить привелегии существующего запроса
	#gr=_execSqlQuery("CREATE EXTENSION IF NOT EXISTS dblink;SELECT * FROM dblink('dbname=%s', 'select alias from  %s.request_sql_list where alias=''%s''') as r(c text)" % (args.appname, args.confname, args.alias), args.appname)
	gr=_execSqlQuery("select alias from  %s.request_sql_list where alias='%s'" % (args.confname, args.alias), args.appname)

	print gr
	if gr and not args.query:
		updatePrivilege2Query(args)
		return[]

	if type(args.query)!=type('') or not args.query:
		print 'ОШИБКА: Необходимо строковым выражением задать шаблон запроса'
		return []
	if type(args.alias)!=type('') or not args.alias:
		print 'ОШИБКА: Необходимо строковым выражением задать псевдоним запроса'
		return []

	# поиск id проекта по его имени
	id_project=_execSqlQuery("select id from  %s.projects where project_name='%s'" % (args.confname, args.project), args.appname)

	if not id_project:
		print "ERROR: Project '%s' is not exists" % (args.project,)
		return []

	priv={}
	if args.group:
		priv=_generatePrivilege(args.group, args.rules)

	_execSqlQuery("INSERT INTO %(config_schema)s.request_sql_list (alias, sql_query, privilege, project_id) VALUES ('%(alias)s', '%(sql_query)s', %(privilege)s, %(project_id)s)" % {'config_schema':args.confname, 'alias':args.alias, 'sql_query':args.query, 'privilege':Json(priv), 'project_id':id_project[0][0]}, args.appname)

	return []

# !!! функция рабочая, но пока не  адаптирована под интерфейс командной строки (см. предыдущие)
def updatePrivilege2Query(args):
	'''
	Function: updatePrivilege2Query
	Summary: Изменяет привелегии запроса. Работает в трех режимах 
			1) привелегии не определены (пустое значение) !!! или ключи заданы неверно - удаление группы привелегии если она есть
			2) привелегии определены, группа существует - привелегии группы обновляются (!! нужно задавать все ключи привелегий) 
			3) привелегии определены, группа не существует - добавляется группа привелегий
	Returns: InsertHere
	'''	
	id_project=_execSqlQuery("select id from  %s.projects where project_name='%s'" % (args.confname, args.project), args.appname)

	if not id_project:
		print "ОШИБКА: Проект '%s' не сущесвует" % args.project
		return []

	priv=_execSqlQuery("select privilege from  %s.request_sql_list where alias='%s' and project_id=%s" % (args.confname, args.alias, id_project[0][0]), args.appname)

	if not priv:
		print "ОШИБКА: Запрос '%s' не сущесвует" % args.alias
		return []
	priv=priv[0][0]

	privilege=_generatePrivilege(args.group, args.rules)
	if args.group in priv:
		if not privilege:
			del(priv[args.group])
			print "INFO: Привелегии '%s' для запроса '%s' были УДАЛЕНЫ" %(args.group, args.alias)
		else:
			print "INFO: Привелегии '%s' для запроса '%s' были ОБНОВЛЕНЫ" %(args.group, args.alias)
	else:
		print "INFO: Привелегии '%s' для запроса '%s' были ДОБАВЛЕНЫ" %(args.group, args.alias)
	priv.update(privilege)

	_execSqlQuery("UPDATE %(config_schema)s.request_sql_list SET privilege = %(privilege)s WHERE alias = '%(alias)s'" % {'config_schema':args.confname, 'alias':args.alias, 'privilege':Json(priv)}, args.appname)

	return priv


#-------ФУНКЦИИ ДЛЯ ПОЛУЧЕНИЯ РАЗЛИЧНОЙ ИНФОРМАЦИИ О КОНФИГУРАЦИИ ПРИЛОЖЕНИЯ (наверное вынести в отдельный файл)

def getObjectInfo(args):
	'''
	Function: getTableFields
	Summary: возвращает инфу об объектах
	Returns: InsertHere
	'''
	r=[]
	if not args.project:
		print "ОШИБКА: Необходимо указать проект"
		return r
	# если таблица не указана в аргументах - выведет инфу о всех таблицах проекта (схемы)
	s=''
	if args.table:
		s= " and table_name='%s'" % args.table
	r=	_execSqlQuery('''
		select table_schema, table_name, column_name, data_type, column_default, is_nullable  
		from information_schema.columns 
		where table_schema='%(project)s'
		and table_schema!='%(confname)s'
		and table_schema!='logs'
		'''  % {'project':args.project, 'confname':args.confname} + s, args.appname)
	return r

def testQueryTemplate(args):
	'''
	Function: testQueryTemplate
	Summary: тестирование шаблона запроса
	Returns: InsertHere
	'''

	f=string.Formatter()
	#--- это - проверка подстановки параметров
	r=''
	try:
		r=f.vformat(args.template, tuple(), args.userparam)
	# если не все аргументы определены
	except KeyError as e:
		print "Key %s is not define ..." % e
		return []
	# если тип значения аргумента не соответсвует типу в строке шаблона 
	except ValueError as e:
		print e
	r=_execSqlQuery(r+' limit 10', args.appname)
	return r
#-------------------------------------------------------------------

# ---------ШАБЛОНЫ ПРЕДСТАВЛЕНИЯ ВОЗВРАЩАЕМЫХ РЕЗУЛЬТАТОВ (рабочий, но в текущей версии скрипта не используется)
def vGetUserApplicationRules(data):
	type_obj={
		'r':'Table',
		'm': 'Materialized view',
		'i': 'Index',
		'S': 'Sequence',
		'v': 'View',
		'c': 'Composite',
		't': 'TOAST table',
		'f': 'Foreign table'
	}

	for r in data:
		r=list(r)
		r[3]=type_obj[r[3]]
		if r[5]:
			r[5]=r[5].replace('{','').replace('}','').split(',')
		print r
#-------------------------------------------------------
from params import *

if __name__=='__main__':	

	parser = argparse.ArgumentParser(description="Скрипт '%(prog)s' добавляет новые объекты в конфигурацию приложения (проекты, пользователей, привилегии, запросы)")

	#---- аргументы общие для всех команд
	parser.add_argument('-a','--appname', help='Имя приложения')
	parser.add_argument('-c', '--confname', default='config', help="Имя схемы конфигурации, по умолчанию - 'config'")

	subparsers = parser.add_subparsers()

	#------- команда создания нового проекта
	parser_create = subparsers.add_parser('create')
	parser_create.add_argument('-u', '--user', required=True)
	parser_create.add_argument('-p', '--password', required=True)
	parser_create.set_defaults(f=createConfigApplication)

	#--------- команда вывода всех бд кластера / проверка прав пользователя для заданной бд
	parser_view = subparsers.add_parser('view')
	parser_view.add_argument('-u', '--user', dest='db_user')
	parser_view.set_defaults(f=getUserApplicationRules)

	#--------- команда добавления
	parser_add = subparsers.add_parser('add')
	parser_add.add_argument('object', choices=['project','group', 'user', 'query'])
	parser_add.add_argument('project')
	parser_add.add_argument('-g', '--group')
	parser_add.add_argument('-u', '--user')
	parser_add.add_argument('-p', '--password', default='')
	parser_add.add_argument('-m', '--mail', default='')
	parser_add.add_argument('--alias', default='')
	parser_add.add_argument('--query', default='')
	parser_add.add_argument('--rules', default='', type=_parseRules)

	def_func={
		'project': addProject,
		'group': addGroup, 
		'user': addUser, 
		'query': addQuery
	}
	parser_add.set_defaults(f='')

	#---вывод инФОРМАЦИИ О БД проекта
	parser_info = subparsers.add_parser('get')
	parser_info.add_argument('-p', '--project')
	parser_info.add_argument('-t', '--table')
	parser_info.set_defaults(f=getObjectInfo)

	#----тестирование шаблона запроса
	parser_test = subparsers.add_parser('test')
	parser_test.add_argument('template')
	parser_test.add_argument('userparam', type=_parseRules)
	parser_test.set_defaults(f=testQueryTemplate)
		
	p=parser.parse_args()
	if 'object' in p:
		p.f=def_func[p.object]	
	
	#------- выполнение
	for i in p.f(p):
		print i
	






