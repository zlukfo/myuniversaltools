-- Cоздаем и настраиваем пользователя, единственного, имеющего доступ к нашему проекту

--1. создаем пользователя (если не существует) и назначаем пароль
do $$
BEGIN
IF not (select '{{db_project_user}}'=any(array(SELECT rolname FROM pg_roles))) THEN
	create role {{db_project_user}} with LOGIN;
	ALTER USER {{db_project_user}} WITH PASSWORD '{{db_project_password}}';
END IF;
END$$;
	

--закрываем доступ к другим базам кластера
-- !!! сделать обязательно - иначе подключившись к любой другой базе (возможность по умолчанию для каждого созданного пользователя)
-- получаем доступ к служебным табицам и функциям схемы pg_catalog (есть во всех бд)
--REVOKE connect ON database ... FROM PUBLIC;

--2. ограничение гупповой роли PUBLIC и как следствие созданному пользователю
-- Забираем у групповой роли PUBLIC права на доступ к таблицам, 
REVOKE ALL PRIVILEGES ON ALL TABLES IN SCHEMA pg_catalog, information_schema FROM PUBLIC;
-- запрещаем для роли public выполнение всех функций
REVOKE ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA {{config_schema}}, logs from public;


-- Предоставляем права пользователю только для нужных объектов
grant select on pg_class, pg_type, pg_database to {{db_project_user}};

-- 3. Настраиваем доступ к logs
GRANT usage, create ON schema logs TO {{db_project_user}};
-- (!! теперь имеется право на создание ЛЮБЫХ объектов в схеме)

alter table logs.log_requests owner to {{db_project_user}};
-- отбиваем ненужные права оставляя только insert
revoke select, UPDATE, DELETE, TRUNCATE, REFERENCES, TRIGGER  ON logs.log_requests from {{db_project_user}};
--таблица содержит счетчик, поэтому открываем доступ к использованию соотвествующей последовательности
grant usage on SEQUENCE logs.log_requests_id_seq to {{db_project_user}};

-- 4. Настраиваем доступ к config
GRANT usage ON schema {{config_schema}} TO {{db_project_user}};
--даем разрешение на выполнение только тем пользователям, которым нужно
GRANT execute ON function {{config_schema}}.getQuery(text, text, text, text) TO {{db_project_user}};
GRANT execute ON function {{config_schema}}.getFromTempl(text, text[]) TO {{db_project_user}};
GRANT execute ON function {{config_schema}}.execQuery(text) TO {{db_project_user}};

-- даем права на просмотр материализованного представления конфига.
grant select on {{config_schema}}.rsl_p_u_pg  to {{db_project_user}};