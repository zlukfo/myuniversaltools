-- функция проверки пользовательского запроса	
	CREATE OR REPLACE FUNCTION {{config_schema}}.getQuery(usr text, passwd text, project text, al text) RETURNS text AS $$
		DECLARE
			r RECORD;
		BEGIN
			 INSERT INTO logs.log_requests(request_time, user_name, request_alias, project, passwd)
    			VALUES (now(), usr, al, project, passwd);
			select * into r from
				(select json_object_keys(privilege) as g_n, group_name, sql_query
				 from {{config_schema}}.rsl_p_u_pg as t
					where
						user_name=usr and
						password_hash=passwd and
						project_name=project and
						alias=al
				) as a
			where g_n=group_name;
		-- если найден запрос для авторизованного пользователя
		if r.sql_query <> '' then
			RETURN r.sql_query;
		end if;
		-- проверяем существут ли такой запрос для анонимного пользователя
			select * into r from
				(select json_object_keys(privilege) as g_n, group_name, sql_query
				 from {{config_schema}}.rsl_p_u_pg as t
					where
						project_name=project and
						alias=al
				) as a
			where g_n='anonymous'
			limit 1;
		-- если существует - возвращаем
		if r.sql_query <> '' then
			RETURN r.sql_query;
		else
			--RAISE EXCEPTION 'Права пользователя не разрешают выпольнить запрос или такого запроса не существует';
			RETURN NULL;
		end if;
		END;
	$$ LANGUAGE plpgsql;