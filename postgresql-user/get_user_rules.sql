-- возвращает права доступа пользователя {{db_project_user}} для объектов бд {{database_name}}
	CREATE EXTENSION IF NOT EXISTS dblink;
	SELECT * FROM dblink('dbname={{database}}', '
		select * from (
		SELECT
		  use.usename as subject,
		  nsp.nspname as schem,
		  c.relname as obj,
		  c.relkind as type,
		  use2.usename as owner,
		  c.relacl,
		  (use2.usename != use.usename and c.relacl::text !~ (''({|,)'' || use.usename || ''='')) as public
		FROM
		  pg_user use
		  cross join pg_class c
		  left join pg_namespace nsp on (c.relnamespace = nsp.oid)
		  left join pg_user use2 on (c.relowner = use2.usesysid)
		WHERE
		  c.relowner = use.usesysid or
		  c.relacl::text ~ (''({|,)(|'' || use.usename || '')='')
		ORDER BY
		  subject,
		  schem,
		  obj
		 ) as res
		 where
		 subject = ''{{db_user}}'';
		 ') 
	AS r(subject name, schem name, obj name, type char, owner name, relacl aclitem[], public boolean);