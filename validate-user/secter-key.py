# -*- coding: utf-8 -*-
# Класс для генерации секретных ключей
# Используется при валидации пользователя в web-приложениях

from datetime import datetime, timedelta
import hashlib
import re

class KeyGenerator():
	def __init__(self, time_shift=2,  salt_position=0, text_array=[], time_point="hour"):
		self.time_shift=time_shift
		self.text_array=text_array
		self.salt_position=salt_position
		if self.salt_position>len(self.text_array): self.salt_position=0

		#по умолчанию точкой отсчета является текущее время создания экземпляра
		#указывать time_point нужно тогда, если мы хотим сделать лействительным ключ не прямо сейчас а в будущем   
		self.time_point=time_point
		if type(self.time_point)!=type('') and  type(self.time_point)!=type(u''):
			print "Точка отсчета времени должна быть задана строкой"
			return

		if time_point=='hour':
			self.time_point=datetime.strftime(datetime.now(), "%Y-%m-%d %H")
		if time_point=='minute':
			self.time_point=datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M")
		if time_point=='day':
			self.time_point=datetime.strftime(datetime.now(), "%Y-%m-%d")



		# рег шаблон определяет три возможные периода обновления ключа
		#  минута, час, день. По моему для многих задач этого достаточно
		pattern=re.compile("(\d{4}-\d{2}-\d{2}) *(\d{2})? *:? *(\d{2})?")
		self.time_point=pattern.match(self.time_point)
		self.time_point=self.time_point.groups() if self.time_point else []
		self.time_point= filter(lambda x: x != None, list(self.time_point))
		
		self._accuracy=len(self.time_point)	# введеная дата при создании экземпляра автоматически
											# определяет период обновления ключа день -1, час -2, минута - 3

		# не сработает, если дата экземпляра введена в неправильном формате 
		if not self.time_point:
			print "Формат даты/времени экземпляра задан неверно"
			return
		
		self.time_point=self._setTimePoint(self.time_point)

		if not self.time_point:
			print "Заданы несуществующие значения даты / времени"
			return

		self.time_point=self._getTimePoints()

		self.time_point=[t.strftime("%Y-%m-%d-%H-%M") for t in self.time_point]
		
	
	# генерирует из внодных значений дату точки отсчета
	def _setTimePoint(self, time_point):
		d_format={
			1:"%Y-%m-%d",
			2:"%Y-%m-%d-%H",
			3:"%Y-%m-%d-%H-%M"
		}
		try:
			return datetime.strptime('-'.join(time_point), d_format[self._accuracy])
		except:
			return None
	
	# генерирует массив временных контрольных точек
	def _getTimePoints(self):
		td=None
		if self._accuracy==1:
			td=timedelta(1)
		if self._accuracy==2:
			td=timedelta(0,0,0,0,0,1)
		if self._accuracy==3:
			td=timedelta(0,0,0,0,1)
		r=[self.time_point+i*td for i in xrange(self.time_shift)]
		return r

	def getEncryptPoints(self):
		if not self.time_point:
			return None
		encrypt_points=[]
		for t in self.time_point:
			d=list(self.text_array)
			d.insert(self.salt_position,t)
			point=hashlib.sha224(''.join(d)).hexdigest()
			encrypt_points.append(point)
		return encrypt_points

	def createUserKey(self):
		d=self.getEncryptPoints()
		if not d: return None
		return d[0]

#---------ПРИМЕР ИСПОЛЬЗОВАНИЯ
k=KeyGenerator(15,2,['id', 'provider', 'email'], time_point='minute')
print k.time_point
print k.getEncryptPoints()
print k.createUserKey()
